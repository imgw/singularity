FROM golang:1.20.0-alpine as builder

################################################################################
#
# Copyright (C) 2019-2023 Vanessa Sochat. Modified by Michael Blaschek.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
################################################################################
# based on: https://github.com/singularityhub/singularity-docker/blob/v3.11.4-slim/Dockerfile
# alpine image with the go tools

RUN apk update && \
    apk add --virtual automake build-base linux-headers libffi-dev
RUN apk add --no-cache bash git openssh gcc squashfs-tools sudo libtool gawk cryptsetup glib-dev
RUN apk add --no-cache linux-headers build-base openssl-dev util-linux util-linux-dev shadow-uidmap libseccomp-dev

ENV APPTAINER_VERSION=1.2.2
LABEL Maintainer @mblaschek
RUN wget -qO- https://github.com/apptainer/apptainer/releases/download/v${APPTAINER_VERSION}/apptainer-${APPTAINER_VERSION}.tar.gz | \
    tar xzv && \
    cd apptainer-${APPTAINER_VERSION} && \
    ./mconfig -p /usr/local/apptainer && \
    make -C builddir && \
    make -C builddir install

# See https://docs.docker.com/develop/develop-images/multistage-build/
# for more information on multi-stage builds.
FROM alpine:3.17
LABEL Maintainer @mblaschek
COPY --from=builder /usr/local/apptainer /usr/local/apptainer
RUN apk add --no-cache ca-certificates libseccomp squashfs-tools tzdata bash && \
    cp /usr/share/zoneinfo/UTC /etc/localtime
ENV PATH="/usr/local/apptainer/bin:$PATH"
ENTRYPOINT ["/usr/local/apptainer/bin/singularity"]
