# Singularity / Apptainer

A repository of definition files for building [Singularity](https://apptainer.org/docs/user/main/introduction.html) containers around the software applications, frameworks, and libraries you need to run on  high-performance computing systems.

Based on [naked-singularity](https://github.com/mkandes/naked-singularity). Thanks M. Kandas

**Please note that since 2022 there is a Linux Foundation Version of singularity called apptainer. Currently developments are similar.**

## Install Singularity / Apptainer

Install Singularity/Apptainer on your Linux desktop, laptop, or virtual machine. You need *root* permissions to use and install the package. Although there is a unprivileged installation possible, [here](https://apptainer.org/docs/admin/main/installation.html#install-unprivileged-from-pre-built-binaries).

Use the instructions from [apptainer](https://apptainer.org/docs/admin/main/installation.html)

```sh title='Apptainer install instructions'
# Red Hat Based
sudo yum install -y epel-release
sudo yum install -y apptainer
# Ubuntu
sudo add-apt-repository -y ppa:apptainer/ppa
sudo apt update
sudo apt install -y apptainer
```

### prebuild binary releases 

If you can not find singularity or apptainer in your repos, there are prebuild packages available as well. Go to the github release [singularity](https://github.com/sylabs/singularity/releases) or [apptainer](https://github.com/apptainer/apptainer/releases) page.

```bash
# e.g. for Ubuntu
wget https://github.com/apptainer/apptainer/releases/download/v1.1.0/apptainer_1.1.0_amd64.deb
sudo dpkg -i apptainer_1.1.0_amd64.deb
# gives some errors of missing dependencies. solve running:
sudo apt-get -f install
# Retry and see that is completes successfully
sudo dpkg -i apptainer_1.1.0_amd64.deb
# done
```

## Build your first Singularity container from a definition file

Build an Ubuntu Singularity container from one of the definition files
available in this repository.

```bash
sudo singularity build ubuntu.sif definition-files/ubuntu/Singularity.ubuntu-18.04
```

## Build your first Singularity container from a DockerHub

Build an Ubuntu Singularity container from the [DockerHub repository](https://hub.docker.com/_/ubuntu)

```bash
# Using Ubuntu 18.04
sudo singularity build ubuntu.sif docker://ubuntu:18.04
# Using Ubuntu 20.04
sudo singularity build ubuntu.sif docker://ubuntu:20.04
```

## Pull your first Singularity container from IMGW Sylab Cloud

We try to build custom Singularity containers with signatures to help keeping track of changes and interoperability.

```bash
# You need to do this only once.
# add the library (no login required)
singularity remote add --no-login sycloud cloud.sylabs.io
# make it the default one
singularity remote use sycloud
# check if you get access
singularity remote status
    INFO:    Checking status of default remote.
    SERVICE    STATUS  VERSION                 URI
    Builder    OK      v1.6.7-0-gebd61128      https://build.sylabs.io
    Consent    OK      v1.6.6-0-gd8f171e       https://auth.sylabs.io/consent
    Keyserver  OK      v1.18.9-0-g76cbd56      https://keys.sylabs.io
    Library    OK      v0.3.7-rc.2-0-gd7bd65e  https://library.sylabs.io
    Token      OK      v1.6.6-0-gd8f171e       https://auth.sylabs.io/token

    No authentication token set (logged out).
# Now pull the container
singularity pull ubuntu.sif library://mblaschek/imgw/ubuntu:20.04 
```

**Note:** With the *pull* command you do **not** need root access. Everybody can pull a container and execute it as ones own process. 


# Inspiration

There are more and more people trying to build/port software to containers and building recipes collections to share experience.
- [Intel MPI - WRF](https://github.com/intel/HPC-containers-from-Intel/tree/master/definitionFiles/WRF)
- [Intel HPC Reference Stack](https://intel.github.io/stacks/hpcrs/README.html) `singularity pull intel-ref-stack.sif docker://sysstacks/hpcrs-centos`
- [Jedi to Go](https://www.jcsda.org/news-blog/2020/6/30/jedi-to-go-high-performance-containers-for-earth-system-prediction)
- [NASA Center for Climate Prediction Containers](https://www.nccs.nasa.gov/sites/default/files/ContainerBestPractices_Rev1.pdf)
- [JEDI in a Container](https://jointcenterforsatellitedataassimilation-jedi-docs.readthedocs-hosted.com/en/latest/learning/tutorials/level1/run-jedi.html) [News-Article](https://www.nccs.nasa.gov/news-events/nccs-highlights/Supercontainers)
- [OpenIFS in Docker](https://confluence.ecmwf.int/display/OIFS/How+to+run+OpenIFS+in+a+Docker+container)
- [ESM Singularity](https://www.fzj.de/SharedDocs/Downloads/IAS/JSC/EN/slides/esm-user-forum-21/06-Reproducible-workflow.pdf)
- [R in singularity - Rocker Project](https://www.rocker-project.org/use/singularity/)

