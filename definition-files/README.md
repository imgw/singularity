
# Development Notes

Building a apptainer

```sh
# just run the build script
./build.sh [recipe]
# build an example
./build.sh Singularity.almalinux.base
# run the final container
./Singularity.almalinux.base.sif
```


## run scripts

```sh title='Example run script to source environment vars'

%runscript
    for script in /.singularity.d/env/*.sh; do
        if [ -f "$script" ]; then
            . "$script"
        fi
    done
    if [ $# -eq 0 ]; then
        exec /bin/bash --norc "$@"
    fi
    exec "$@"

```