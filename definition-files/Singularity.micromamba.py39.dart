Bootstrap: docker
From: mambaorg/micromamba:latest

%labels
    maintainer IT-IMGW <it.img-wien@univie.ac.at>

%files
    ./runscript /.singularity.d/runscript
    ./run-help /.singularity.d/runscript.help

%post
    apt -y update && apt -y install \
        build-essential \
        git \
        tcsh \
        csh \
        ksh \
        m4 \
        libtool \
        automake \
        autoconf \
    && apt -y clean
    # needed to update to openmpi versions
    micromamba -q install -y -n base -c conda-forge \
        eccodes=2.27.0 \
        cdo \
        nco \
        netcdf4=1.6.1=mpi_openmpi_py39ha5e07e6_0 \
        hdf5=1.12.2=mpi_openmpi_h41b9b70_0 \
        gcc \
        gfortran \
        python=3.9.13 &&
    /opt/conda/bin/pip install eccodes && /opt/conda/bin/pip cache purge &&
    micromamba clean --all --yes
    # command prompt name
    CNAME=m.dart
    # does not work goes into /.singularity.d/env/91-environment.sh 
    echo "export PS1=\"[IMGW-$CNAME]\w\$ \"" >> /.singularity.d/env/99-zz-custom-env.sh
    # add some labels
    echo "libc $(ldd --version | head -n1 | cut -d' ' -f4)" >> "$SINGULARITY_LABELS"
    echo "linux $(cat /etc/os-release | grep PRETTY_NAME | cut -d'=' -f2)" >> "$SINGULARITY_LABELS"

%test
    python -m eccodes selfcheck
    gfortran --version
    gcc --version
    ncks --version
    cdo --version

%environment
    export PATH=/opt/conda/bin:$PATH
    export LD_LIBRARY_PATH=/opt/conda/lib:/usr/lib64:/lib64:/lib
    export LIBRARY=/opt/conda/lib:/usr/lib64:/lib64:/lib
    export INCLUDE=/opt/conda/include:/usr/include
