Bootstrap: docker
From: mambaorg/micromamba:latest

%labels
    maintainer IT-IMGW <it.img-wien@univie.ac.at>

%files
    ./runscript /.singularity.d/runscript
    ./run-help /.singularity.d/runscript.help


%apprun mpitest
    nproc=4
    if [ $# -eq 1 ]; then
        nproc=$1
    fi
    echo "Running MPITest : $nproc"
    exec mpirun -np $nproc mpitest.x

%appfiles mpitest
    ./src/mpitest.c ./mpitest.c

%appinstall mpitest
    export PATH=/opt/conda/bin:$PATH
    export LIBRARY=/opt/conda/lib
    export INCLUDE=/opt/conda/include
    mpicc mpitest.c -o mpitest.x

%environment
    export LANG=C.UTF-8
    export PATH=/opt/conda/bin:$PATH
    export LIBRARY=/opt/conda/lib
    export INCLUDE=/opt/conda/include

%post
    micromamba -q install -y -n base -c conda-forge \
        python=3.9 \
        ucx \
        openmpi \
        mpi4py \
    && micromamba clean --all --yes
    # command prompt name
    CNAME=m.ompi
    # does not work goes into /.singularity.d/env/91-environment.sh 
    echo "export PS1=\"[IMGW-$CNAME]\w\$ \"" >> /.singularity.d/env/99-zz-custom-env.sh
    # add some labels
    echo "libc $(ldd --version | head -n1 | cut -d' ' -f4)" >> "$SINGULARITY_LABELS"
    echo "linux $(cat /etc/os-release | grep PRETTY_NAME | cut -d'=' -f2)" >> "$SINGULARITY_LABELS"

%test
    mpicc --version

