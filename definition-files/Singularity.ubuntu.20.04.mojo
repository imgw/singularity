Bootstrap: docker
From: ubuntu:20.04

%labels
    maintainer IT-IMGW <it.img-wien@univie.ac.at>

%files
    ./runscript /.singularity.d/runscript
    ./run-help /.singularity.d/runscript.help
    ./src/mojokernel.py /modular/pkg/packages.modular.com_mojo/jupyter/kernel/mojokernel2.py

%post
    export DEFAULT_TZ=Vienna/Europe
    export DEBIAN_FRONTEND='noninteractive'
    apt-get update \
    && apt-get install -y \
        tzdata \
        vim \
        curl \
        wget \
        g++ \
        make \
        file \
        git \
        python3-venv \
        apt-transport-https \
        libedit2
    rm -rf /var/lib/apt/lists/*
    mkdir -p /modular

    export MODULAR_HOME="/modular"
    export PATH="$MODULAR_HOME/pkg/packages.modular.com_mojo/bin:$PATH"

    curl https://get.modular.com | sh -
    apt-get install -y modular
    modular --help
    modular auth $MOJO_AUTH
    modular install mojo

    # Cleanup
    apt-get -y autoremove --purge
    apt-get -y clean

    # add a environment script for initalizing
    cat > /.singularity.d/env/99-mojo.sh<<EOF
mkdir -p \$HOME/.modular
cp -u /modular/modular.cfg \$HOME/.modular/
if [ ! -d \$HOME/.modular/pkg ]; then
    ln -s /modular/pkg \$HOME/.modular/pkg
fi
EOF
    # create a jupyter kernel that can be used.
    mkdir -p /modular/pkg/packages.modular.com_mojo/venv/share/jupyter/kernels/mojo-jupyter-kernel
    cp /root/.local/share/jupyter/kernels/mojo-jupyter-kernel/* /modular/pkg/packages.modular.com_mojo/venv/share/jupyter/kernels/mojo-jupyter-kernel/
    cat > /modular/pkg/packages.modular.com_mojo/venv/share/jupyter/kernels/mojo-jupyter-kernel/kernel.json <<EOF
{
  "display_name": "Mojo",
  "argv": [
    "python3",
    "/modular/pkg/packages.modular.com_mojo/jupyter/kernel/mojokernel2.py",
    "-f",
    "{connection_file}"
  ],
  "language": "mojo",
  "codemirror_mode": "mojo",
  "language_info": {
    "name": "mojo",
    "mimetype": "text/x-mojo",
    "file_extension": ".mojo",
    "codemirror_mode": {
      "name": "mojo"
    }
  }
}
EOF
    # fix environment for additional packages
    sed -i 's/false/true/' /modular/pkg/packages.modular.com_mojo/venv/pyvenv.cfg
    # command prompt name
    CNAME=u20.mojo
    # does not work goes into /.singularity.d/env/91-environment.sh 
    echo "export PS1=\"[IMGW-$CNAME]\w\$ \"" >> /.singularity.d/env/99-zz-custom-env.sh
    # add some labels
    echo "libc $(ldd --version | head -n1 | cut -d' ' -f4)" >> "$SINGULARITY_LABELS"
    echo "linux $(cat /etc/os-release | grep PRETTY_NAME | cut -d'=' -f2)" >> "$SINGULARITY_LABELS"

%environment
    export LC_ALL=C
    export LANG=C.UTF-8
    export LIBRARY=/opt/conda/lib:/usr/lib64:/lib64:/lib
    export INCLUDE=/opt/conda/include:/usr/include
    export MODULAR_HOME="$HOME/.modular"
    export PATH="$MODULAR_HOME/pkg/packages.modular.com_mojo/bin:$MODULAR_HOME/pkg/packages.modular.com_mojo/venv/bin:/opt/conda/bin:$PATH"
    export SHELL=/bin/bash


# DOCKERFILE:
# FROM ubuntu:20.04

# ENV DEBIAN_FRONTEND='noninteractive'
# RUN apt-get update \
#     && apt-get install -y curl g++ make file python3-venv apt-transport-https libedit2\
#     && mkdir -p /modular
# ENV LC_ALL=C
# ENV LANG=C.UTF-8
# ENV LIBRARY=/usr/lib64:/lib64:/lib
# ENV INCLUDE=/usr/include
# ENV MODULAR_HOME="/modular"
# ENV PATH="$MODULAR_HOME/pkg/packages.modular.com_mojo/bin:$PATH"

# RUN curl https://get.modular.com | sh - \
#     && apt-get install -y modular
# RUN modular auth $MOJO_AUTH \
#     && modular install mojo
#     # Cleanup
# RUN apt-get -y autoremove --purge\
#     && apt-get -y clean
