# Development Containers

We are going to use spack to build a container with different libraries and headers inside and will produce different stages.


## Define a spack configuration
We can choose some libraries and version that we need and build it into a concise image. We choose ubuntu 18.04 and spack 0.16.3, but we can build our own OS and spack version as we further invest.

This needs to be in a file called spack.yaml 
```yaml
spack:
  specs:
  - hdf5@1.10.7+mpi target=skylake_avx512
  - openmpi@3.1.6 target=skylake_avx512

  container:
    images:
      os: "ubuntu:18.04"
      spack: 0.16.3

    format: singularity

    strip: true

    os_packages:
      final:
      - gcc
      - gcc-gfortran

    labels:
      app: "hdf5@1.10.7+mpi"
      mpi: "openmpi@3.1.6"
```

micro architectures known to spack:

```
Generic architectures (families)
    aarch64  arm  ppc  ppc64  ppc64le  ppcle  riscv64  sparc  sparc64  x86  x86_64  x86_64_v2  x86_64_v3  x86_64_v4

GenuineIntel - x86
    i686  pentium2  pentium3  pentium4  prescott

GenuineIntel - x86_64
    nocona  nehalem   sandybridge  haswell    skylake  cannonlake      cascadelake
    core2   westmere  ivybridge    broadwell  mic_knl  skylake_avx512  icelake

AuthenticAMD - x86_64
    k10  bulldozer  piledriver  zen  steamroller  zen2  zen3  excavator

IBM - ppc64
    power7  power8  power9

IBM - ppc64le
    power8le  power9le

Cavium - aarch64
    thunderx2

Fujitsu - aarch64
    a64fx

ARM - aarch64
    graviton  graviton2

Apple - aarch64
    m1

SiFive - riscv64
    u74mc
```

convert this configuration into a singularity (apptainer) recipe

```bash
spack containerize > Singularity.hdf5.1.10.7
```

which has the following content

```singularity
Bootstrap: docker
From: spack/ubuntu-bionic:0.16.3
Stage: build

%post
  # Create the manifest file for the installation in /opt/spack-environment
  mkdir /opt/spack-environment && cd /opt/spack-environment
  cat << EOF > spack.yaml
spack:
  specs:
  - hdf5@1.10.7+mpi
  - openmpi@3.6.1
  concretization: together
  config:
    install_tree: /opt/software
  view: /opt/view
EOF

  # Install all the required software
  . /opt/spack/share/spack/setup-env.sh
  spack env activate .
  spack install --fail-fast
  spack gc -y
  spack env deactivate
  spack env activate --sh -d . >> /opt/spack-environment/environment_modifications.sh

  # Strip the binaries to reduce the size of the image
  find -L /opt/view/* -type f -exec readlink -f '{}' \; | \
    xargs file -i | \
    grep 'charset=binary' | \
    grep 'x-executable\|x-archive\|x-sharedlib' | \
    awk -F: '{print $1}' | xargs strip -s


Bootstrap: docker
From: ubuntu:18.04
Stage: final

%files from build
  /opt/spack-environment /opt
  /opt/software /opt
  /opt/view /opt
  /opt/spack-environment/environment_modifications.sh /opt/spack-environment/environment_modifications.sh

%post
  # Update, install and cleanup of system packages needed at run-time
  apt-get -yqq update && apt-get -yqq upgrade
  apt-get -yqq install gcc gfortran
  rm -rf /var/lib/apt/lists/*
  # Modify the environment without relying on sourcing shell specific files at startup
  cat /opt/spack-environment/environment_modifications.sh >> $SINGULARITY_ENVIRONMENT

%labels
  app hdf5@1.10.7+mpi
  mpi openmpi@3.1.6
```

Then we can use that recipes to build a singularity container:

```bash
singularity build hdf5.1.10.7.sif Singularity.hdf5.1.10.7
```



## Build your first Singularity container for spack development and integration

This is a much more complex build and of course configure example. We will build a Centos 7 image (or use any other) and install [spack](https://spack.readthedocs.io/en/latest/), a HPC package manager, into the container with our new favorite compiler. In a next step we can use that container and install even more packages into a development container for different apps.

```bash
# this will take some time ...
singularity build centos7.sif definition-files/centos/Singularity-centos7
# resulting in a 180 MB Centos root image, called centos.sif
singularity build centos7.spack.sif definition-files/centos/Singularity-centos7-spack
```
The next step is to use the [definition file](definition-files/centos/Singularity-centos7-spack) and install the wanted compiler into that container

```bash
...
# using spack for Installing
$SPACK_ROOT/bin/spack install -y gcc@8.4.0
# adding the compiler to the settings
$SPACK_ROOT/bin/spack compiler add $(spack location -i gcc@8.4.0)
...
```
Now we have an image that includes spack version 0.17.3 with a recent gcc @8.4.0 compiler, ready for development.

```bash

```

### Containerizing applications using spack


## Signing and hosting containers

```bash
singularity sign 
```