#!/bin/bash
# By MB
# Build the required SPACK container images locally

CPATH=$(dirname $0)
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m'

report() {
    if [ $? -eq 0 ]; then
        printf "%-68s[$GREEN%10s$NC]\n" "$@" "OK"
        return 0
    else
        printf "%-68s[$RED%10s$NC]\n" "$@" "FAILED"
        return 1
    fi
}

centos7def() {
    cat <<EOF >Singularity.centos7.$gccversion
Bootstrap: docker
From: spack/centos7:v${spackversion}
Stage: build

%post
  # takes about 1 hour to build
  # Create the manifest file for the installation in /opt/spack-environment
  mkdir /opt/spack-environment && cd /opt/spack-environment
  cat << EOM > spack.yaml
spack:
  specs:
  - gcc@${gccversion}
  concretization: together
  config:
    install_tree: /opt/software
  view: /opt/view
EOM

  # Install all the required software
  . /opt/spack/share/spack/setup-env.sh
  spack env activate .
  spack install --fail-fast
  spack compiler add $(spack location -i gcc@${gccversion})
  spack gc -y
  spack env deactivate
  spack env activate --sh -d . >> /opt/spack-environment/environment_modifications.sh

  # Strip the binaries to reduce the size of the image
  find -L /opt/view/* -type f -exec readlink -f '{}' \; | \
    xargs file -i | \
    grep 'charset=binary' | \
    grep 'x-executable\|x-archive\|x-sharedlib' | \
    awk -F: '{print \$1}' | xargs strip -s


Bootstrap: docker
From: centos:7
Stage: final

%files from build
  /opt/spack-environment /opt
  /opt/software /opt
  /opt/spack /opt
  /opt/view /opt
  /opt/spack-environment/environment_modifications.sh /opt/spack-environment/environment_modifications.sh

%post
  # Modify the environment without relying on sourcing shell specific files at startup
  cat /opt/spack-environment/environment_modifications.sh >> \$SINGULARITY_ENVIRONMENT
EOF
}

alma8def() {
    cat <<EOF >Singularity.alma8.$gccversion
# Bootstrap: library
# From: mblaschek/spack/alma8:${spackversion}
Bootstrap: localimage
From: alma8-spack${spackversion}
Stage: build

%post
  # takes about 1 hour to build
  # Create the manifest file for the installation in /opt/spack-environment
  mkdir /opt/spack-environment && cd /opt/spack-environment
  cat << EOM > spack.yaml
spack:
  specs:
  - gcc@${gccversion}
  concretization: together
  config:
    install_tree: /opt/software
  view: /opt/view
EOM

  # Install all the required software
  . /opt/spack/share/spack/setup-env.sh
  spack env activate .
  spack install --fail-fast
  spack compiler add $(spack location -i gcc@${gccversion})
  spack gc -y
  spack env deactivate
  spack env activate --sh -d . >> /opt/spack-environment/environment_modifications.sh

  # Strip the binaries to reduce the size of the image
  find -L /opt/view/* -type f -exec readlink -f '{}' \; | \
    xargs file -i | \
    grep 'charset=binary' | \
    grep 'x-executable\|x-archive\|x-sharedlib' | \
    awk -F: '{print \$1}' | xargs strip -s

# Bootstrap: library
# From: mblaschek/spack/alma8:${spackversion}
Bootstrap: localimage
From: alma8-spack${spackversion}
Stage: final

%files from build
  /opt/spack-environment /opt
  /opt/software /opt
  /opt/spack /opt
  /opt/view /opt
  /opt/spack-environment/environment_modifications.sh /opt/spack-environment/environment_modifications.sh

%post
  # Modify the environment without relying on sourcing shell specific files at startup
  cat /opt/spack-environment/environment_modifications.sh >> \$SINGULARITY_ENVIRONMENT
EOF
}

question() {
	read -p "$@ (y/n)" yn
	case $yn in
	[Yy]*) return 0 ;;
	*)
		warning "skipping"
		return 1
		;;
	esac
}

# Requires sudo/root privileges
test $EUID -eq 0
report "Requires sudo/root privileges" || exit 1
status=$(type singularity)
report "Singularity: $status" || exit 1
echo "Singularity Version: $(singularity version)"
# Find Recipes
DEFPATH=$(realpath $CPATH/../..)/definition-files
test -d $DEFPATH
report "Definitions directory" || exit 1
echo "Skipping CentOS 7 builds ... supplied by spack"
# Check declared Functions
echo "Checking functions ..."
declare -F | grep -v "\-fx" | cut -d" " -f3 | awk '{print "- ", $0}'
echo ""
# for two stage builds use:
# Bootstrap: docker   -> localimage
# From: spack/centos7:v0.17.3  -> centos7-spack
# Stage: build

# for final
# Bootstrap: docker   -> localimage
# From: centos:7    -> centos7-dev
# Stage: final

spackversion=
read -p "Spack Version (default: 0.17.3): " spackversion
if [ -z "$spackversion" ]; then
    spackversion=0.17.3
fi
gccversion=
read -p "GCC Compiler Version (default: 8.4.0): " gccversion
if [ -z "$gccversion" ]; then
    gccversion=8.4.0
fi
#
# Build AlmaLinux spack container
#
test -f alma8-spack${spackversion}
report "AlmaLinux 8 spack image available"
if [ $? -ne 0 ]; then
    test -f $DEFPATH/almalinux/Singularity-alma8-spack
    report "AlmaLinux 8 spack definition found" || exit 1
    sed "s/v0.17.3/v${spackversion}/" $DEFPATH/almalinux/Singularity-alma8-spack >Singularity-alma8-spack
    singularity build alma8-spack${spackversion} Singularity-alma8-spack
    report "AlmaLinux 8 spack container ready" || exit 1
fi
#
# Build Centos 7 GCC build container
#
echo "Building CentOS 7 with spack $spackversion and gcc $gccversion"
test -f centos7-spack${spackversion}-gcc${gccversion}
report "Singularity Image: centos7-spack${spackversion}-gcc${gccversion}"
question "Build Centos 7 spack with gcc $gccversion ?"
if [ $? -eq 0 ]; then
    echo "Preparing definitions ... "
    centos7def
    test -f Singularity.centos7.$gccversion
    report "Definitions ready." || exit 1
    echo "Building ..."
    singularity build centos7-spack${spackversion}-gcc${gccversion} Singularity.centos7.$gccversion
    report "Singularity CentOS 7 image $spackversion / $gccversion"
fi
#
# Build AlmaLinux 8 GCC build container
#
test -f alma8-spack${spackversion}-gcc${gccversion}
report "Singularity Image: alma8-spack${spackversion}-gcc${gccversion}"
if [ $? -ne 0 ]; then
    echo "Preparing definitions ... "
    alma8def
    test -f Singularity.alma8.$gccversion
    report "Definitions ready." || exit 1
    echo "Building ..."
    singularity build alma8-spack${spackversion}-gcc${gccversion} Singularity.alma8.$gccversion
    report "Singularity AlmaLinux 8 image $spackversion / $gccversion"
fi
# singularity build centos8-spack-gcc${gccversion} Singularity.centos8.$gccversion
#
# Verify containers work
# not as root
singularity key list | grep IMGW
report "IMGW GPG key" || exit 1
echo "Please supply GPG passphrase for your key"
singularity sign alma8-spack${spackversion}
singularity sign alma8-spack${spackversion}-gcc${gccversion}
singularity sign centos7-spack${spackversion}-gcc${gccversion}
#
# Upload to cloud / repository for open access
#
# Spack primary development container
singularity push alma8-spack${spackversion} library://mblaschek//spack${spackversion}/alma8:8.5.0
# Spack development container with gcc versions
singularity push alma8-spack${spackversion}-gcc${gccversion} library://mblaschek//spack${spackversion}/alma8:${gccversion}
singularity push centos7-spack${spackversion}-gcc${gccversion} library://mblaschek//spack${spackversion}/centos7:${gccversion}
# singularity push centos8-spack-icc${iccversion} library://mblaschek/spack/alma8/icc:${iccversion}
