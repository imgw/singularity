# WRF in a container


:construction:

This is still experimental. No guarantees.


Please find the following containers available for download:
- WRF
- GSI
- WRFpy
- WRF.dev


## Design

build a container with all requirements installed and make it easily available to users to further develop. We will use a two stage build to complete the development


1. WRF sandbox / requirements
2. Build container
3. Final container

Build some special containers, that are most requested, e.g. em_real and em_les


## Example: Hurricane Sandy

## Performance tests

We ran the same container:
- VSC
- JET
- SRVX1

Multi-Node?

# Development

If you like to build your own WRF container with customized source code or a different setting/purpose, then you can find here some guidance on how to do this.

Steps:
1. Pull the WRF development container from
2. Pull the source code of WRF / WPS ...
3. Configure the Source code to use the libraries inside the container
4. Test your settings / compilation
5. Build your own container from the WRF development container and your SRC Code.


# Intel
intel compiled WRF is faster
intel-oneapi-runtime is 1.3GB
intel-onaapi-compilers 10 GB