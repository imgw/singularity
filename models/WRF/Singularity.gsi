Bootstrap: localimage
From: sandbox.wrf.dev
Stage: build

# Two Stage Build of GSI
# use the sandbox development image
# 1. Compile the target
# 2. Copy relevant exe to target GSI container
# Make sure the ldd / linking and inputs are ok

%labels
maintainer IT-IMGW <it.img-wien@univie.ac.at>

%files
./scripts/runscript /.singularity.d/runscript
./scripts/run_gsi.ksh /wrf/bin

%apprun init
echo "Please modify run_gsi.ksh to your needs and settings or use set_env.ksh"
cp -uv /wrf/bin/run_gsi.ksh .

%apphelp init
Use this app to copy the run_gsi.ksh to your current directory.
This is a modified version from dtcenter container version.
Please update either the run_gsi.ksh or the set_env.ksh in your local directory.

%post
# GSI requires cmake and openblas + lapack
yum -y install cmake openblas-devel.x86_64 openblas-openmp.x86_64
# Build a WRF release from the sandbox
WRF_BUILD_TARGET=em_real
LDFLAGS="-lm"
NETCDF=/usr/lib64/openmpi/
HDF5_ROOT=$NETCDF

export GSI_VERSION=3.7
export ENKF_VERSION=1.3

mkdir -p /wrf/gsi \
&& curl -SL https://dtcenter.org/sites/default/files/comGSIv${GSI_VERSION}_EnKFv${ENKF_VERSION}.tar.gz | tar -xzC /wrf/gsi
#
#
#
ln -s /usr/lib64/gfortran/modules/openmpi/* /usr/lib64/openmpi/include/
#
# prep GSI build
#
mkdir -p /wrf/gsi/build \
&& cd /wrf/gsi/build \
&& cmake /wrf/gsi/comGSIv${GSI_VERSION}_EnKFv${ENKF_VERSION}
#
# Fix a few GSI bugs
#
RUN umask 0002 \
 && sed -i 's/wij(1)/wij/g' /wrf/gsi/comGSIv3.7_EnKFv1.3/src/setuplight.f90 \
 && sed -i "s,\$, -L$NETCDF,g" /wrf/gsi/build/src/CMakeFiles/gsi.x.dir/link.txt \
 && sed -i "s,\$, -L$NETCDF,g" /wrf/gsi/build/src/enkf/CMakeFiles/enkf_wrf.x.dir/link.txt
#
# Build GSI
#
cd /wrf/gsi/build \
&& make -j 4
#
# Fix all wired paths
#
# sed -i 's,/comsoftware/gsi/,/wrf/gsi/' /wrf/bin/run_gsi.ksh \
# && sed -i 's,/gsi_build,/build' /wrf/bin/run_gsi.ksh \
# && sed -i 's,/data,./data,g' /wrf/bin/run_gsi.ksh \
# && sed -i 's,/home/scripts/case,.,g' /wrf/bin/run_gsi.ksh \
# && sed -i 's,/home/wrfprd,.,g' /wrf/bin/run_gsi.ksh \
# && sed -i 's,/home/gsiprd,.,g' /wrf/bin/run_gsi.ksh 
#
# Wrap it up
#
echo "export GSI_VERSION=$GSI_VERSION" >> $SINGULARITY_ENVIRONMENT
echo "export ENKF_VERSION=$ENKF_VERSION" >> $SINGULARITY_ENVIRONMENT
CNAME=alma8.gsi
# does not work goes into /.singularity.d/env/91-environment.sh 
echo "export PS1=\"[IMGW-$CNAME]\w\$ \"" >> /.singularity.d/env/99-zz-custom-env.sh

%environment
export PATH=/wrf/gsi/build/bin:/wrf/bin:$PATH