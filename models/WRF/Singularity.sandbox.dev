Bootstrap: localimage
From: alma8.base.sif

%labels
maintainer IT-IMGW <it.img-wien@univie.ac.at>
baseimage AlmaLinux8
wrfversion 4.4.1

%post
WRF_VERSION=4.4.1
WPS_VERSION=4.4
#
# WRF install packages / requirements from repo
#
# Every line will be a layer in the container
# See https://fedoraproject.org/wiki/EPEL#Quickstart for powertools
# yum --enablerepo epel groupinstall -y "Development Tools" \
yum update -y \
&& yum install -y dnf-plugins-core \
&& dnf config-manager --set-enabled powertools \
&& yum install -y epel-release \
&& yum update -y \
&& yum --enablerepo epel install -y \
        curl \
        file \
        findutils \
        gcc-c++ \
        gcc \
        gcc-gfortran \
        glibc.i686 libgcc.i686 \
        libpng-devel jasper-libs jasper-devel \
        m4 make perl \
        tar tcsh time which zlib zlib-devel \
        git \
        gnupg2 \
        hostname \
        iproute \
        patch \
        openmpi-devel \
        openmpi \
        hdf5-openmpi-devel \
        hdf5-openmpi-static \
        netcdf-openmpi-devel \
        netcdf-openmpi-static \
        netcdf-fortran-openmpi-devel \
        netcdf-fortran-openmpi-static \
&& rm -rf /var/cache/yum \
&& yum clean all \
&& dnf clean all \
&& rm -rf /usr/share/doc \
&& rm -rf /usr/share/man \
&& ln -s /usr/include/openmpi-x86_64/ /usr/lib64/openmpi/include

# WRF root directory
mkdir -p /wrf/WRF /wrf/WPS
# Download the WRF version from Github
curl -SL https://github.com/wrf-model/WRF/releases/download/v${WRF_VERSION}/v${WRF_VERSION}.tar.gz | tar --no-same-owner --strip-components=1 -zxC /wrf/WRF \
&& curl -SL https://github.com/wrf-model/WPS/archive/refs/tags/v${WPS_VERSION}.tar.gz | tar --no-same-owner --strip-components=1 -zxC /wrf/WPS

# command prompt name
CNAME=wrf.sandbox
# does not work goes into /.singularity.d/env/91-environment.sh 
echo "export PS1=\"[IMGW-$CNAME]\w\$ \"" >> /.singularity.d/env/99-zz-custom-env.sh
# add to default environment
echo "export WRF_VERSION=$WRF_VERSION" >> $SINGULARITY_ENVIRONMENT
echo "export WPS_VERSION=$WPS_VERSION" >> $SINGULARITY_ENVIRONMENT
# not sure why that does not happen as default
echo "export PKG_CONFIG_PATH=/usr/lib64/openmpi/lib/pkgconfig/" >> $SINGULARITY_ENVIRONMENT


%environment
export LD_LIBRARY_PATH=/usr/lib64/openmpi/lib:/usr/lib64:/lib64:/lib
export PATH=/usr/lib64/openmpi/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
export LIBRARY=/usr/lib64/openmpi/lib:/usr/lib64:/lib64:/lib
export INCLUDE=/usr/include/openmpi-x86_64/:/usr/lib64/gfortran/modules/openmpi:/usr/include
export NETCDF=/usr/lib64/openmpi
export NETCDF_ROOT=/usr/lib64/openmpi
export JASPERINC=/usr/include/jasper/
export JASPERLIB=/usr/lib64/
