Bootstrap: docker
From: mambaorg/micromamba:latest

%labels
maintainer IT-IMGW <it.img-wien@univie.ac.at>

%files
../../definition-files/runscript /.singularity.d/runscript
../../definition-files/run-help /.singularity.d/runscript.help

%post
# install curl
apt -y update && apt -y install curl && apt -y clean
# install python packages
micromamba -q install -y -n base -c conda-forge \
    pygrib=2.1.4 \
    cartopy=0.21 \
    netCDF4=1.5.8 \
    pyyaml=6.0
# include WRF/WPS scripts
mkdir -p /opt/wrf && cd /opt/wrf
curl -sL https://github.com/NCAR/container-dtc-nwp/archive/refs/tags/v4.1.0.tar.gz | tar --strip-components=5 -zxC . container-dtc-nwp-4.1.0/components/scripts/common/python/
# fix wired paths
sed -i 's,/home/pythonprd,./,g' *.py
sed -i 's,/home/postprd/,./,g' *.py
sed -i 's,/home/scripts/case/,./,g' *.py
sed -i 's,cartopy.config,#cartopy.config,g' *.py

# final
CNAME=wrfpy
# does not work goes into /.singularity.d/env/91-environment.sh 
echo "export PS1=\"[IMGW-$CNAME]\w\$ \"" >> /.singularity.d/env/99-zz-custom-env.sh
# add some labels
echo "libc $(ldd --version | head -n1 | cut -d' ' -f4)" >> "$SINGULARITY_LABELS"
echo "linux $(cat /etc/os-release | grep PRETTY_NAME | cut -d'=' -f2)" >> "$SINGULARITY_LABELS"

%environment
export MPLCONFIGDIR=.
export LANG=C.UTF-8
export PATH=/opt/conda/bin:$PATH
export LIBRARY=/opt/conda/lib
export INCLUDE=/opt/conda/include
export PYTHONDONTWRITEBYTECODE=1
export PYTHONPATH=/opt/wrf:$PYTHONPATH
