#!/bin/bash
# By MB
# Based on the Example from DTCenter, Boulder NCAR.
# https://dtcenter.org/nwp-containers-online-tutorial
# https://dtcenter.org/nwp-containers-online-tutorial/hurricane-sandy-case-27-oct-2012

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m'

pause() {
    if $DEBUG; then
        read -n1 -rsp $'Press any key to continue or Ctrl+C to exit...\n'
    fi
}

warning() {
    printf "%-68s[$YELLOW%10s$NC]\n" "$@" "SKIPPED"
    return 0
}

report() {
    if [ $? -eq 0 ]; then
        printf "%-68s[$GREEN%10s$NC]\n" "$@" "OK"
        return 0
    else
        printf "%-68s[$RED%10s$NC]\n" "$@" "FAILED"
        return 1
    fi
}

question() {
    read -p "$@ (y/n)" yn
    case $yn in
    [Yy]*) return 0 ;;
    *)
        warning "skipping"
        return 1
        ;;
    esac
}

INAME="SANDY"
CPATH=$(dirname $0)
case_name=sandy
WRFCONTAINER=$CPATH/WRF-4.4.1-em_real.sif
PYCONTAINER=$CPATH/WRFpy.sif
GISCONTAINER=$CPATH/GSI-3.7.sif
NPROC=1
if [ $NPROC -gt 1 ]; then
    warning "[$INAME] Trying to run with n=$NPROC processes."
    echo "[$INAME] Make sure mpirun can be accessed!"
    command -v mpirun
    report "[$INAME] mpirun command works" || exit 1
    # Check if Versions are similar ?
    # MPI Version outside needs to be newer!
    
fi

# Check if we have the container?

# test -f Makefile
# report "[$INAME] Makefile found"
# if [ $? -eq 0 ]; then
#     echo "[$INAME] Building WRF ..."
#     make em_real
# fi

# Do a pull request from the IMGW library to retrieve the container
test -f $WRFCONTAINER
report "[$INAME] WRF em_real [$WRFCONTAINER]"
if [ $? -ne 0 ]; then
    # Try to pull
    echo "Manual: https://cloud.sylabs.io/library/mblaschek/models/wrf"
    singularity pull $WRFCONTAINER library://mblaschek//models/wrf-emreal:4.4.1
    report "[$INAME] WRF em_real pulled from library" || exit 1
fi

test -f $PYCONTAINER
report "[$INAME] WRF python [$PYCONTAINER]" || exit 1

test -f $GISCONTAINER
report "[$INAME] GIS [$GISCONTAINER]" || exit 1

PROJ_PATH=
read -p "[$INAME] Where do you want to place all files? [$PWD] :" PROJ_PATH
if [ -z "$PROJ_PATH" ]; then
    PROJ_PATH=.
fi

INPUT_DIR=$PROJ_PATH/data

mkdir -vp $PROJ_PATH/sandy/data/
report "[$INAME] using $PROJ_PATH"
cd $PROJ_PATH/sandy/data/
# curl -SL https://dtcenter.ucar.edu/dfiles/container_nwp_tutorial/tar_files/container-dtc-nwp-derechodata_20120629.tar.gz | tar zxC .
# curl -SL https://dtcenter.ucar.edu/dfiles/container_nwp_tutorial/tar_files/container-dtc-nwp-snowdata_20160123.tar.gz | tar zxC .

test -d model_data
report "[$INAME] Sandy input data"
if [ $? -ne 0 ]; then
    curl -SL https://dtcenter.ucar.edu/dfiles/container_nwp_tutorial/tar_files/container-dtc-nwp-sandydata_20121027.tar.gz | tar zxC .
    report "[$INAME] using Sandy input data" || exit 1
fi
test -d obs_data
report "[$INAME] Sandy obs data"
if [ $? -ne 0 ]; then
    curl -SL https://dtcenter.ucar.edu/dfiles/container_nwp_tutorial/tar_files/CRTM_v2.3.0.tar.gz | tar zxC .
    report "[$INAME] using Sandy obs data" || exit 1
fi
test -d WPS_GEOG
report "[$INAME] Sandy geogrid data"
if [ $? -ne 0 ]; then
    curl -SL https://dtcenter.ucar.edu/dfiles/container_nwp_tutorial/tar_files/wps_geog.tar.gz | tar zxC .
    report "[$INAME] using Sandy geogrid data" || exit 1
fi
test -d shapefiles
report "[$INAME] Sandy natural earth data"
if [ $? -ne 0 ]; then
    curl -SL https://dtcenter.ucar.edu/dfiles/container_nwp_tutorial/tar_files/shapefiles.tar.gz | tar zxC .
    report "[$INAME] using Sandy natural earth data" || exit 1
fi
# return back
cd $PROJ_PATH/sandy

#
# dtcenter
# https://github.com/NCAR/container-dtc-nwp
#

#
# Setup Case files for SANDY
#
# Get sandy case files
curl -sL https://github.com/NCAR/container-dtc-nwp/archive/refs/tags/v4.1.0.tar.gz | tar --strip-components=4 -zxC . container-dtc-nwp-4.1.0/components/scripts/sandy_20121027
# fix paths
sed -i 's,/data/,./data/,g' set_env.ksh
sed -i 's,/data/,./data/,g' namelist.wps
sed -i 's,/comsoftware/wrf/WPS-4.3,./g' namelist.wps

# Optional run plot
# needs cartopy and python
# /home/scripts/common/run_python_domain.ksh

#
# Get default em_real configuration files from /wrf/run
# WRF/run and WPS/geogrid. WPS/metgrid, WPS/ungrib
#
echo "[$INAME] CP em_real files to current directory [no overwriting]"
./$WRFCONTAINER init
report "[$INAME] WRF init complete" || exit 1

# Check if Variable Tables are present / depends on input files
source set_env.ksh
# input_data must be sourced
test -d Vtable.${input_data}
report "[$INAME] Varible Table Vtable.${input_data} found" || {echo "Please provide Variable Table!"
exit 1}
# Link Grib files to here
link_grib.csh $INPUT_DIR/model_data/${case_name}/*
report "[$INAME] Model input data from: $INPUT_DIR/model_data/${case_name}/"

#
# geogrid data
#
# Remove old files
if [ -e geo_em.d*.nc ]; then
    rm -rf geo_em.d*.nc
fi

# Command for geogrid
echo "[$INAME] Running geogrib ..."
singularity exec $WRFCONTAINER geogrid.exe >run_geogrid.log 2>&1

test -f geo_em.d01.nc
report "[$INAME] WPS geogrid ready." || exit 1

#
# ungrib data
#
file_date=$(cat namelist.wps | grep -i start_date | cut -d"'" -f2 | cut -d":" -f1)
# remove old files
if [ -e PFILE:${file_date} ]; then
    rm -rf PFILE*
fi
if [ -e FILE:${file_date} ]; then
    rm -rf FILE*
fi

# Command for ungrib
echo "[$INAME] Running ungrib ..."
singularity exec $WRFCONTAINER ungrib.exe >run_ungrib.log 2>&1

ls -ls FILE:*
report "[$INAME] WPS ungrib ready." || exit 1

#
# metgrid data
#

# Remove old files
if [ -e met_em.d*.${file_date}:00:00.nc ]; then
    rm -rf met_em.d*
fi

# Command for metgrid
echo "[$INAME] Running metgrid ..."
singularity exec $WRFCONTAINER metgrid.exe >run_metgrid.log 2>&1

ls -ls met_em.d01.*
report "[$INAME] WPS metgrid ready." || exit 1

echo "[$INAME] WPS setup complete. Ready for WRF"

#
# WRF
#

sed -e '/nocolons/d' namelist.input >nml
cp namelist.input namelist.nocolons
mv nml namelist.input

# Remove old files
if [ -e wrfinput_d* ]; then
    rm -rf wrfi* wrfb*
fi

# Command for real
echo "[$INAME] Running real ..."
# This can be run with MPI or serial?
if [ $NPROC -gt 1 ]; then
    # MPI
    module load openmpi/
    mpirun -np $NPROC singularity exec $WRFCONTAINER real.exe >run_real.log 2>&1
else
    # Serial run
    singularity exec $WRFCONTAINER real.exe >run_real.log 2>&1
fi
test -f wrfinput_d01 && test -f wrfbdy_d01
report "[$INAME] WRF run finished"

#
# GSI Data Assimilation
#
if false; then
    echo "[$INAME] Running gsi ..."
    singularity run --app init $GISCONTAINER
    test -f run_gsi.ksh
    report "[$INAME] GSI run script ready" || exit 1
    # add the correct run Command
    if [ $NPROC -gt 1 ]; then
        echo "export RUN_COMMAND=\"mpirun -np $NPROC singularity exec $GISCONTAINER \$GIS_EXE\"" >>set_env.ksh
    else
        echo "export RUN_COMMAND=\"singularity exec $GISCONTAINER \$GIS_EXE\"" >>set_env.ksh
    fi
    # Execute GSI
    ./run_gsi.ksh
fi
#
# run WRF
#
# If wrfinput_d01.orig exists, rename it to wrfinput_d01 to reset the state
if [[ -e wrfinput_d01.orig ]]; then
    mv wrfinput_d01.orig wrfinput_d01
fi

# If GSI was run, update the wrfinput file
if [[ -e ./wrf_inout ]]; then
    mv wrfinput_d01 wrfinput_d01.orig
    cp ./wrf_inout wrfinput_d01
fi
if [ $NPROC -gt 1 ]; then
    mpirun -np $NPROC singularity exec $WRFCONTAINER wrf.exe >run_wrf.log 2>&1
else
    singularity exec $WRFCONTAINER wrf.exe >run_wrf.log 2>&1
fi
