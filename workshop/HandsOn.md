# Hands-On

In order to build a singularity container you require a **root** environment, such as on a
- Personal computer (Linux) -  (development: Windows, Mac) - [Instructions](https://sylabs.io/guides/3.0/user-guide/installation.html#overview)
- Virtual Machine (all OS) - VMWare, KVM, VirtualBox - install a recent Linux and install singularity
- Build Service - [Sylab Cloud](https://cloud.sylabs.io/home) - advanced


## IMGW Resources
Singularity is installed on all IMGW servers and on VSC4 via module (`module load singularity`). Most other HPC environments have a version installed as well.

Based on a repository from M. Kandas, there is an installation routine 
```bash
git clone https://gitlab.phaidra.org/imgw/singularity.git 
sudo ./install-singularity.sh install
```
and there are example **definition-files** that help to build containers by themselves. These files are called *Receipes*. It is of course by design that these receipes can be shared and adopted to ones needs. If you have a good one. Send it to us. Thanks. :)


## Using a VM to build containers

We are using a VM to build our containers with root privileges.
Steps:
- Connect to JET: `ssh user@jet01.img.univie.ac.at`
- Connect to the VM: `ssh rocky##@192.168.122.230`
- clone this repo into your VMs HOME directory: `git clone https://gitlab.phaidra.org/imgw/singularity.git`

Please note that you will recieve the username and the password in the course.

## Singularity Tutorial

There are a few things that you need to learn about singularity.

```bash
singularity [options] <subcommand> [subcommand options] ...
```

has three essential subcommands:
- **build**: Build your own container from scratch using a Singularity definition file; download and assemble any existing Singularity container; or convert your containers from one format to another (e.g., from Docker to Singularity).
- **shell**: Spawn an interactive shell session in your container.
- **exec**: Execute an arbitrary command within your container.
- **run**: Execute the runscript

### Definition File

Typically a Singularity Recipe or Definition files is made of different [sections](https://sylabs.io/guides/3.3/user-guide/definition_files.html#sections):
- *Bootstrap* - start source
- *Files* - Files to copy into the container
- *Post* - install instructions
- *Labels* - Information for the User
- *Environment* - Runtime environment variables
- *Help* - Runtime help information
- *Run* - Runtime instructions, script, executable
- *Apps* - Special executables defined as apps
- *Setup* - DO not use.
- *Test* - Tests to check if the container runs optimal

Please note that the *Bootstrap* can be changed for example, if you build the ubuntu container already, e.g. ubuntu.sif, then downloading that container image again might be not efficient. Therefore, replacing the Bootstrap is a nice option.
```bash
#Bootstrap: library
#From: mblaschek/imgw/ubuntu:18.04
Bootstrap: localimage
From: ubuntu.sif
```
Please have a look at the other options -  [Other bootstrap agents @ sylab](https://sylabs.io/guides/3.3/user-guide/definition_files.html#other-bootstrap-agents)

Let's take for example the [miniconda3](../definition-files/miniconda/Singularity.miniconda3-py39-4.9.2-ubuntu-18.04) Recipe and investigate what it does.

```bash
Bootstrap: library
From: mblaschek/imgw/ubuntu:18.04


%labels

    APPLICATION_NAME miniconda3
    APPLICATION_VERSION py39-4.9.2-Linux-x86_64
    APPLICATION_URL https://docs.conda.io

    AUTHOR_NAME Michael Blaschek
    AUTHOR_EMAIL michael.blaschek@univie.ac.at

    LAST_UPDATED 20211118

%setup

%environment

    # Set the conda distribution type, its version number, the python
    # version it utilizes, the root and installation directories where
    # the distribution will be installed within the container, and the
    # root URL to the installer
    export CONDA_DISTRIBUTION='miniconda'
    export CONDA_VERSION='3'
    export CONDA_PYTHON_VERSION='py39'
    export CONDA_INSTALLER_VERSION='4.9.2'
    export CONDA_ARCH='Linux-x86_64'
    export CONDA_INSTALL_DIR="/opt/${CONDA_DISTRIBUTION}${CONDA_VERSION}"

    # Set PATH to conda distribution
    export PATH="${CONDA_INSTALL_DIR}/bin:${PATH}"

%post -c /bin/bash

    # Set operating system mirror URL
    export MIRRORURL='http://at.archive.ubuntu.com/ubuntu'

    # Set operating system version
    export OSVERSION='bionic'

    # Set system locale
    export LC_ALL='C'

    # Set debian frontend interface
    export DEBIAN_FRONTEND='noninteractive'

    # Upgrade all software packages to their latest versions
    apt-get -y update && apt-get -y upgrade

    cd /tmp

    # Set the conda distribution type, its version number, the python
    # version it utilizes, the root and installation directories where
    # the distribution will be installed within the container, and the
    # root URL to the installer
    export CONDA_DISTRIBUTION='miniconda'
    export CONDA_VERSION='3'
    export CONDA_PYTHON_VERSION='py39'
    export CONDA_INSTALLER_VERSION='4.9.2'
    export CONDA_ARCH='Linux-x86_64'
    export CONDA_INSTALLER="${CONDA_DISTRIBUTION^}${CONDA_VERSION}-${CONDA_PYTHON_VERSION}_${CONDA_INSTALLER_VERSION}-${CONDA_ARCH}.sh"
    export CONDA_INSTALL_DIR="/opt/${CONDA_DISTRIBUTION}${CONDA_VERSION}"
    export CONDA_ROOT_URL='https://repo.anaconda.com'

    # Download and install conda distribution
    wget "${CONDA_ROOT_URL}/${CONDA_DISTRIBUTION}/${CONDA_INSTALLER}"
    chmod +x "${CONDA_INSTALLER}"
    "./${CONDA_INSTALLER}" -b -p "${CONDA_INSTALL_DIR}"

    # Remove conda installer
    rm "${CONDA_INSTALLER}"

    # Cleanup
    apt-get -y autoremove --purge
    apt-get -y clean

    # Update database for mlocate
    updatedb

%files

%runscript

%test
```

### Examples

#### COW?
One of the most common examples to show how to create a singularity container is the lolcow:
```bash
# Using Sylab library
sudo singularity build lolcow.sif library://sylabs-jms/testing/lolcow
# Using Docker
sudo singularity build lolcow.sif docker://godlovedc/lolcow
```
And then run it:
```bash
# Invoke the buildin runscript:
singularity run lolcow.sif
# using execute
singularity exec lolcow.sif sh -c "fortune | cowsay | lolcat"
 ______________________________________
/ Don't tell any big lies today. Small \
\ ones can be just as effective.       /
 --------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```
As you can see it can be useful to have a runscript. Please have a look at the runscript:
```bash
Bootstrap: docker
From: ubuntu:16.04

%post
    apt-get -y update
    apt-get -y install fortune cowsay lolcat

%environment
    export LC_ALL=C
    export PATH=/usr/games:$PATH

%runscript
    fortune | cowsay | lolcat
```
But what if I do not know what the runscript is anymore?
```bash
# Try Help ?
singularity run-help lolcow.sif 
No help sections were defined for this image
# Look inside:
singularity shell lolcow.sif
Singularity> cat /.singularity.d/runscript
#!/bin/sh
OCI_ENTRYPOINT='"/bin/sh" "-c" "fortune | cowsay | lolcat"'
OCI_CMD=''
# ENTRYPOINT only - run entrypoint plus args
if [ -z "$OCI_CMD" ] && [ -n "$OCI_ENTRYPOINT" ]; then
    SINGULARITY_OCI_RUN="${OCI_ENTRYPOINT} $@"
fi
...
```
Of course everything needs to be **in** the container. 


#### Miniconda

Let's build something that might actually be useful.
1. We are going to build the default *miniconda3* example from the *definition-files*
2. Then we are going to add some python packages that you might want to use and see if they are installed inside.

```bash
# Build the default image
sudo singularity build miniconda3.sif definition-files/miniconda/Singularity.miniconda3-py39-4.9.2-ubuntu-18.04
# Check that it can run:
singularity exec miniconda3.sif python3 --version
Python 3.9.1
```
Now please open the definition file using e.g. 
```bash
# change to the git repository (clone with cmd above)
cd $HOME/singularity
# Copy
cp definition-files/miniconda/Singularity.miniconda3-py39-4.9.2-ubuntu-18.04 definition-files/miniconda/Singularity.miniconda3-py39-4.9.2-ubuntu-18.04-custom
# Edit
vim definition-files/miniconda/Singularity.miniconda3-py39-4.9.2-ubuntu-18.04-custom
# Build again
sudo singularity build miniconda3-custom.sif definition-files/miniconda/Singularity.miniconda3-py39-4.9.2-ubuntu-18.04-custom
# Test
singularity exec miniconda3-custom.sif python3
```

#### MPI Example (Advanced)
Please only try to do this when you have time. Usually the building of OpenMPI takes some time, e.g. 5 min (VM).
There are three examples:
- Using Centos 8 and Intel MPI (untested)
- Using Ubuntu 18.04 and OpenMPI @ 4.0.5 
- Using Rocky Linux 8.4 and OpenMPI @ git

##### Using Ubuntu
```bash
# Building
sudo singularity build ubuntu-OMPI.sif definition-files/MPI/Singularity.ubuntu-18.04-OMPI-gcc
# load the MPI from the VM or JET
module load mpi/openmpi-x86_64
# Running using 4 Cores
mpirun -np 4 singularity exec ubuntu-OMPI.sif /opt/mpitest
module purge
```

##### Using Rocky
```bash
# Building
sudo singularity build rocky-OMPI.sif definition-files/rocky/Singularity.rocky-8.4-OMPI
# load the MPI from the VM or JET
module load mpi/openmpi-x86_64
# Running using 4 Cores
mpirun -np 4 singularity exec rocky.sif /usr/bin/mpi_ring
module purge
```
[Sylab MPI Documentation](https://sylabs.io/guides/3.3/user-guide/mpi.html)


### Singularity Variables

#### Cache, TMP
On Servers you might be limited (file size, quota, ...) by the definition of your `SINGULARITY_CACHEDIR` (`singularity cache clean`) or your  `SINGULARITY_TMPDIR` directory.  


Limited space in home directories.
Set to `$TMPDIR` to avoid quota limits.

```bash
export SINGULARITY_CACHEDIR=$TMPDIR
export SINGULARITY_TMPDIR=$TMPDIR
```

#### Bind
In order to include host paths into your container use the `SINGULARITY_BIND` variable defined in the host system or provide the runtime command the option `-b paths` or `--bind paths`

e.g.
```bash
# Directory does not exist by default
singularity exec my_container.sif ls /data
ls: cannot access '/data': No such file or directory
# Binding /data on host to /data inside
singularity exec --bind /data:/data my_container.sif ls /data
log.txt
```

or using the environment variables:

```bash
# on the HOST system
export SINGULARITY_BIND=/data:/data
# execute singularity with no --bind options
singularity exec my_container.sif ls /data
log.txt
```

#### PATH
Of course it is possible to modify the `PATH` that is accessible from the container.

```bash
export SINGULARITYENV_PREPEND_PATH=/opt/important/bin
export SINGULARITYENV_APPEND_PATH=/opt/fallback/bin
export SINGULARITYENV_PATH=/only/bin
```

#### Clean Environment
Singularity by default exposes all environment variables from the host inside the container. Use the `--cleanenv` argument to prevent this:
```bash
singularity run --cleanenv <image.sif> <arg-1> <arg-2> ... <arg-N>
```

#### Setting Environment variables
One can define an environment variable within the container as follows:
```bash
export SINGULARITYENV_MYVAR=Overridden
```
With the above definition, `MYVAR` will have the value "Overridden". 

### Inspect

One can sometimes learn a lot about the image by inspecting its definition file:

```bash
singularity inspect --deffile <image.sif>
```

The definition file is the recipe by which the image was made (see below). If the image was taken from Docker Hub then a definition file will not be available.

