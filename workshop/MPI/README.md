# MPI Tests with Python

Based on a course from Ivan Kondov in [VSC Trainigs](https://gitlab.phaidra.org/imgw/trainings-course/-/blob/master/HPC%20with%20Python/docs/03_parallel_part_1.md#exercise-3-installation-and-setup-of-the-mpi4py-package) there are some easy tests that can be run with singularity containers and MPI.

This requires to use the container built by the definition file:
[`definition-files/MPI/Singularity.miniconda3-py39-4.9.2-ubuntu-18.04-OMPI`](../../definition-files/MPI/Singularity.miniconda3-py39-4.9.2-ubuntu-18.04-OMPI)


## Example mpi4py

There are some builtin tests witht mpi4py package to test its functionality, e.g. the ringtest

```bash
mpirun -np 4 singularity exec miniconda3-ompi.sif python -m mpi4py.bench ringtest -n 1024 -l 1000
time for 1000 loops = 0.00760765 seconds (4 processes, 1024 bytes)
```
or hello world
```bash
mpirun -np 4 singularity exec miniconda3-ompi.sif python -m mpi4py.bench helloworld
Hello, World! I am process 0 of 4 on manjaro.
Hello, World! I am process 1 of 4 on manjaro.
Hello, World! I am process 2 of 4 on manjaro.
Hello, World! I am process 3 of 4 on manjaro.
```


## Example Pi Statistics
based on an example from [Cornell University](https://cvw.cac.cornell.edu/python/exercise) - Monte Carlo with mpi4py 

![](https://cvw.cac.cornell.edu/python/images/throwingdarts.png)
Randomly thrown darts: red dots are those darts that land in the unit circle, and blue dots are those that do not.

Fortunately, even though you are not a very good dart thrower, you are a good random number generator, and you can put those skills to work to estimate the numerical value of pi — the ratio of the circumference of a circle to its diameter.

```bash
# Install additional python packages not installed in our container
# Will be installed to .local/lib/python3.9/site-packages/
singularity exec miniconda3-ompi.sif python -m pip install numpy matplotlib
Defaulting to user installation because normal site-packages is not writeable
Collecting matplotlib
...
Successfully installed cycler-0.11.0 fonttools-4.28.2 kiwisolver-1.3.2 matplotlib-3.5.0 numpy-1.21.4 packaging-21.3 pillow-8.4.0 pyparsing-3.0.6 setuptools-scm-6.3.2 tomli-1.2.2
# Now run the script with MPI
mpirun -np 4 singularity exec miniconda3-ompi.sif python parallel_pi.py
MPI size = 4
1024 3.1505126953125 0.06090403395917971
4096 3.1389007568359375 0.025047770509458944
16384 3.1422119140625 0.012053709369960034
65536 3.141084671020508 0.006269848947240971
262144 3.142267942428589 0.0035848687877291755
1048576 3.1418583393096924 0.0019135112002101906
4194304 3.1416348814964294 0.0008085763712888695
16777216 3.1416458263993263 0.0004231117067632255
67108864 3.141561470925808 0.00018093953356869197
268435456 3.14160884777084 9.47003093073785e-05
```

Which produces two figures:
![](pi_vs_log2_N.png)
Numerical esimate of pi as a function of how many darts are thrown (log2 scale). Error bars reflect the standard deviation in the estimate over multiple independent runs.

![](log2_std_vs_log2_N.png)

Scaling of the fluctuations in the estimate of pi (log2(standard deviation)) as a function of log2(N)
