# Singularity Workshop
@IMGW

**Date:** 24.11.2021

**Lecturer:** @blaschm7

## Summary
Introduction to Singularity and its applications on HPC. The workshop aims at bringing the tools to the users and trying first hand what can be done. Of course this is just a tiny introduction to a much larger topic.

Literature:
- [Singularity: Scientific containers for mobility of compute](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0177459)
- Cloud Computing for Science and Engineering - Book - [UB](https://web-p-ebscohost-com.uaccess.univie.ac.at/ehost/detail/detail?vid=0&sid=6dbe42f5-29a9-4256-bfc6-bece2b66646e%40redis&bdata=JnNpdGU9ZWhvc3QtbGl2ZQ%3d%3d#AN=2517979&db=nlebk) 
- [A container for HPC](https://www.admin-magazine.com/HPC/Articles/Singularity-A-Container-for-HPC)

## Schedule

| Time | Task |
| --- | --- |
| 10:30 - 11:00 | [Introduction to Singularity](Introduction.pdf) |
| 11:00 - 11:30 | [Hands-on](HandsOn.md), build a container |
| 11:30 - 12:00 | [Problems](Introduction-Advanced.pdf), advanced examples |


